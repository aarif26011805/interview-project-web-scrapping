
class PathConversion:

    @staticmethod
    def html_path_generator(file_name):
        result = file_name + '.html' 
        return result 
    
    @staticmethod
    def text_path_generator(file_name):
        result = file_name + '.txt' 
        return result 