# import modules
from bs4 import BeautifulSoup 
import requests
from urllib.request import Request, urlopen
from englisttohindi.englisttohindi import EngtoHindi
from utility import PathConversion


headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'}
req = Request("https://www.classcentral.com/",headers=headers)
html_page = urlopen(req)
soup = BeautifulSoup(html_page, "lxml")

links = []
for link in soup.findAll('a'):
    links.append(link.get('href'))

def scrapper():
    for i in links:
        if 'https://' not in i:
            converted_links = 'https://www.classcentral.com'+str(i)
            page = requests.get(converted_links,headers=headers)
            connvert_to_html = BeautifulSoup(page.text, "html.parser")
            with open(PathConversion.html_path_generator(file_name=str(i).replace('/','')), "a", encoding="utf-8") as f:
                    f.write(str(connvert_to_html))
            for script in connvert_to_html(["script", "style"]):
                script.extract()    # rip it out
            text = connvert_to_html.get_text()
            lines = (line.strip() for line in text.splitlines())
            chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
            text = '\n'.join(chunk for chunk in chunks if chunk)
            with open(PathConversion.text_path_generator(file_name=str(i).replace('/','')), "a", encoding="utf-8") as f:
                     f.write(str(text))
            try:
                content = EngtoHindi(text)
                convert = content.convert
                with open('hindi.txt', "a", encoding="utf-8") as f:
                     f.write(str(convert))
            except:
                pass
        else:
            page = requests.get(i,headers=headers)
            connvert_to_html = BeautifulSoup(page.text, "html.parser")
            with open(PathConversion.html_path_generator(file_name=str(i).replace('/','')), "a", encoding="utf-8") as f:
                    f.write(str(connvert_to_html))

            for script in connvert_to_html(["script", "style"]):
                script.extract()    # rip it out
            text = connvert_to_html.get_text()
            lines = (line.strip() for line in text.splitlines())
            chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
            text = '\n'.join(chunk for chunk in chunks if chunk)
            with open(PathConversion.text_path_generator(file_name=str(i).replace('/','')), "a", encoding="utf-8") as f:
                     f.write(str(text))
            try:
                content = EngtoHindi(text)
                convert = content.convert
                with open('hindi.txt', "a", encoding="utf-8") as f:
                     f.write(str(convert))
            except:
                pass
scrapper()



